<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="java.util.ArrayList" import="java.util.List" 
    import="jakarta.servlet.http.HttpSession" 
    import="com.examen.fernandoq.beans.ComprasSingleton" 
    import="com.examen.fernandoq.beans.ComprasSingletonLocal"
    import="jakarta.servlet.http.Cookie" %>
<%@ include file="menu.jsp" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Ventas</title>
</head>
<body>
	<center><h1>Formulario de Ventas</h1></center>
	<form action="VentasController" method="post">
		<center>
			<div>
				<label>Nit:</label>
				<input type="text" name="nit" value="0"/>
			</div><br>
			<div>
				<label>Apellido:</label>
				<input type="text" name="apellido" value=""/>
			</div><br>
			<div>	
				<select name="producto">
				<% 
				String[][] productos=new String[][]{};
				productos=(String[][])request.getAttribute("productos");
				for (String[] val : productos) { %>
					<option value="<% out.println(val[0]); %>"><% out.println(val[1]); %></option>
				<% } %>
				</select>
				<input type="number" min="1" name="cantidad" value="1">
			</div><br>
			<input type="submit" value="Agregar Producto">
		</center>
	</form>
	<br><br>
	<!-- <form action="ComprasController" method="post">
		<input type="submit" value="Comprar">
	</form> -->
	
	<%
	HttpSession carritoSession = request.getSession();
	List<String[]> carritoVenta = new ArrayList<String[]>();

	if(carritoSession.getAttribute("carrito")==null) {
		%>
	
	<%}else{%>
	<center>
		<table border="1">
			<tr>
				<td>Nro.</td>
				<td>Nombre de Producto.</td>
				<td>Precio.</td>
				<td>Cantidad.</td>
			</tr>
			<%
			//carritoVenta = (List<String[]>) carritoSession.getAttribute("carrito");
			//ComprasSingletonLocal ejb2 = new ComprasSingleton();
			//carritoVenta = ejb2.ver();
			carritoVenta = (List<String[]>)request.getAttribute("ventas");
			int cont=0;
			for (String[] cadena : carritoVenta) {
				//out.println(cadena[1]);
				cont++;
				%>
			<tr>
				<td><% out.println(cont); %></td>
				<td><% out.println(cadena[0]); %></td>
				<td><% out.println(cadena[1]); %></td>
				<td><% out.println(cadena[2]); %></td>
			</tr>
				<%	
			}		
				%>		
		</table>
	</center>
	<%}%>
</body>
</html>