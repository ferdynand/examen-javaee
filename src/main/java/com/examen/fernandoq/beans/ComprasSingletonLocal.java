package com.examen.fernandoq.beans;

import java.util.ArrayList;
import java.util.List;

import jakarta.ejb.Local;

@Local
public interface ComprasSingletonLocal {
	List<String[]> agregar(String[] val);
	List<String[]> ver();
	
}
