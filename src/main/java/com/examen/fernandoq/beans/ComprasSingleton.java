package com.examen.fernandoq.beans;

import java.util.ArrayList;
import java.util.List;

import jakarta.ejb.LocalBean;
import jakarta.ejb.Singleton;

/**
 * Session Bean implementation class ComprasSingleton
 */
@Singleton
@LocalBean
public class ComprasSingleton implements ComprasSingletonLocal {

    List<String[]> carrito = new ArrayList<String[]>(); 
    public ComprasSingleton() {
        // TODO Auto-generated constructor stub
    }

	@Override
	public List<String[]> ver() {
		return carrito;
	}

	@Override
	public List<String[]> agregar(String[] val) {
		carrito.add(val);
		return carrito;
	}

}
