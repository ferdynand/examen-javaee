package com.examen.fernandoq.controllers;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.examen.fernandoq.beans.ComprasSingleton;
import com.examen.fernandoq.beans.ComprasSingletonLocal;

import jakarta.servlet.RequestDispatcher;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;

/**
 * Servlet implementation class ComprasController
 */
public class ComprasController extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private ComprasSingletonLocal ejb = new ComprasSingleton();
    /**
     * Default constructor. 
     */
    public ComprasController() {
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub

		HttpSession carrito = request.getSession();
		List<String[]> valor = new ArrayList<String[]>();
		List<String[]> valorCompras = new ArrayList<String[]>();
		valor = (List<String[]>) carrito.getAttribute("carrito");
		//valor = ejb.ver();
		String[] nits = new String[9999999]; 
		float totalFull=0F;
		for (String[] val : valor) {
			float total = 0F;
			boolean entrar=true;
			int cont1 = 0;
			for(int i=0;i<nits.length;i++) {
				if(val[3].equals(nits[i])) {
					entrar=false;
		//			System.out.println("entro");
				}
			}
			if(entrar==true) {
				nits[cont1]=val[3];
				cont1++;
				for (String[] val2 : valor) {
					if(val[3].equals(val2[3])) {
						total = total + (Float.parseFloat(val2[1])*Integer.parseInt(val2[2]));
						totalFull = totalFull + (Float.parseFloat(val2[1])*Integer.parseInt(val2[2]));
						valorCompras.add(val2);
					}
				}
				String[] subvalor = new String[]{"sub total","" + total,"", "", "", ""};
				valorCompras.add(subvalor);
			}
			
		}
		String[] subvalorF = new String[]{"total",totalFull + "","", "", "", ""};
		valorCompras.add(subvalorF);
		
		carrito.setAttribute("carritoVentas", valorCompras);
		RequestDispatcher rd = request.getRequestDispatcher("compra.jsp");
		rd.forward(request, response);
	}

}
