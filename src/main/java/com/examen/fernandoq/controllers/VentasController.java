package com.examen.fernandoq.controllers;

import java.io.IOException;
import java.io.PrintWriter;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

import com.examen.fernandoq.beans.ComprasSingleton;
import com.examen.fernandoq.beans.ComprasSingletonLocal;

import jakarta.servlet.RequestDispatcher;
import jakarta.servlet.ServletContext;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;

/**
 * Servlet implementation class VentasController
 */
public class VentasController extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private ComprasSingletonLocal ejb = new ComprasSingleton(); 
    /**
     * Default constructor. 
     */
    public VentasController() {
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String[][] productos=new String[][]{
			{"1","Producto 1","20.50"},
			{"2","Producto 2","22.50"},
			{"3","Producto 3","23.50"},
			{"4","Producto 4","24.50"},
			{"5","Producto 5","25.00"},
			{"6","Producto 6","26.00"}
		};
		
		PrintWriter out = response.getWriter();
		List<String[]> valor = new ArrayList<String[]>(); 
		List<String[]> valor2 = new ArrayList<String[]>(); 
		List<String[]> valor3 = new ArrayList<String[]>();

		HttpSession carrito = request.getSession();
//		HttpSession nit = request.getSession();
//		HttpSession apellido = request.getSession();
		String producto = request.getParameter("producto");
		String cantidadProducto = request.getParameter("cantidad");
//		System.out.println(producto);
		//"nombre producto","precio","cantidad"};
		String nit = "";
		String apellido = "";
		String fecha="";
		try {
			nit = request.getParameter("nit");
			apellido = request.getParameter("apellido");
			fecha = LocalDateTime.now().format(DateTimeFormatter.ISO_DATE_TIME);
		} catch (Exception e) {
			// TODO: handle exception
		}
		valor = ejb.ver();
//		if(carrito.getAttribute("carrito")!=null) {
//			valor = (List<String[]>) carrito.getAttribute("carrito");
//		}
		for (String[] val : productos) {
//			System.out.println("---" + val[0] + "---" + producto + "---");
			try {
				if(Integer.parseInt(val[0].trim())==Integer.parseInt(producto.trim())) {
//					System.out.println("entro");
					String[] subvalor = new String[]{val[1],val[2],cantidadProducto, nit, apellido, fecha};
//					System.out.println(subvalor[4] + "-" + subvalor[5]);
					//valor.add(subvalor);
					ejb.agregar(subvalor);
				}
			} catch (Exception e) {
				// TODO: handle exception
			}
		}		
		carrito.setAttribute("carrito", ejb.ver());
		//carrito.setAttribute("carrito", valor);
		
//		System.out.println(valor);
//		String nombreArticulo="";
//		Float precioArticulo=0F;
//		int cantidadArticulo=0;
		
//		try {
//			nit.setAttribute("nit", nit);
//			apellido.setAttribute("nit", apellido);
//		} catch (Exception e) {
//			// TODO: handle exception
//		}
		
//		if(carrito.getAttribute("carrito")==null) {
//			carrito.setAttribute("carrito", valor);
//			valor2 = (List<String[]>) carrito.getAttribute("carrito");
//			for (String[] cadena : valor2) {
//				out.println(cadena[1]);
//			}			
//		}else {
//			out.println("ya hay product");
//		}
		//response.sendRedirect("venta.jsp");
		request.setAttribute("productos", productos);
		request.setAttribute("ventas", ejb.ver());
		RequestDispatcher rd = request.getRequestDispatcher("venta.jsp");
		rd.forward(request, response);
		
		//carrito.setAttribute("carrito", valor);
		// TODO Auto-generated method stub
//		valor2 = (List<String[]>) carrito.getAttribute("carrito");
//		ServletContext context = request.getSession().getServletContext();
//		context.setAttribute("carrito_aux", valor);
//		valor3 = (List<String[]>) context.getAttribute("carrito_aux");
//		doGet(request, response);
	}

}
