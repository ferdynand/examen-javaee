package com.examen.fernandoq.controllers;

import java.io.IOException;
import java.util.Iterator;

import jakarta.servlet.RequestDispatcher;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.Cookie;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;

/**
 * Servlet implementation class autenticacionController
 */
public class autenticacionController extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * Default constructor. 
     */
    public autenticacionController() {
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String usuario="", password="";
		String[][] usuariosRegistrados=new String[][]{
			{"1","admin","admin","administrador"},
			{"2","cajero","123", "cajero de ventas"}
		};
		HttpSession nombreUsuario = request.getSession();
		boolean usuarioVerifica=false, passwordVerifica=false;
		try {
			usuario = request.getParameter("user");
			password = request.getParameter("password");	
			for (String[] val : usuariosRegistrados) {
				if(usuario.equals(val[1])) {
					if(password.equals(val[2])) {
						usuarioVerifica=true;
						passwordVerifica=true;
						Cookie cookieUsuario = new Cookie("usuario",usuario);
						Cookie cookiePassword = new Cookie("password",password);
//						Cookie cookieNombreUsuario = new Cookie("nombreUsuario",val[3]);
						nombreUsuario.setAttribute("nombreUsuario", val[3]);
						response.addCookie(cookieUsuario);
						response.addCookie(cookiePassword);
//						response.addCookie(cookieNombreUsuario);
					}
				}
			}
		} catch (Exception e) {
			// TODO: handle exception
		} 
		if(usuarioVerifica==true && passwordVerifica==true) {
			RequestDispatcher rd = request.getRequestDispatcher("/VentasController");
			rd.forward(request, response);
		}else {
			RequestDispatcher rd = request.getRequestDispatcher("index.html");
			rd.forward(request, response);
		}
	}

}
